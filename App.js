import React,{ Component } from 'react';
import { Image, StatusBar, StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import Camera from 'react-native-camera';




export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
        qrcode: ''
    }

}

componentWillMount() {
  console.log('here is your current state of qrcode', this.state.qrcode);
}

onBarCodeRead = async(e) => {
  const upcCodeForWebsite = e;
  console.log('here is your upccode man', upcCodeForWebsite);
  this.setState({qrcode: e.data});
  try {
    const upcOnly = await fetch('http://ptsv2.com/t/nux3o-1532064409/post', {
        method: 'POST',
        body: JSON.stringify({
          upc: e.data
        })
    });
    if (upcOnly.status === 200) {
      const bodyresponse = await upcOnly.text();
      if(bodyresponse !== '') {
        console.log('here is your response,', bodyresponse);
      }
      
    }
    console.log('oh..not yet', upcOnly);
  } catch (ee) {
    console.log('couldnt send this upc to a website', ee);
  }
};

render () {
    return (
        <View  style={styles.container}>
            <Camera
                style={styles.preview}
                onBarCodeRead={this.onBarCodeRead}
                ref={cam => this.camera = cam}
                aspect={Camera.constants.Aspect.fill}
                >
                    <Text style={{
                        backgroundColor: 'white'
                    }}>{this.state.qrcode}</Text>
                </Camera>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  }
});