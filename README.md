# barcodeScanner

This is a simple barcode scanner that uses rn 0.53.0 and react-native-camera 0.13.0

Originally cloned from 
https://github.com/hungdev/React-Native-Camera-Demo



They key to making this work is manually installing/linking react-native-camera.

in MainApplication.java add the line 

import com.lwansbrough.RCTCamera.RCTCameraPackage;

after line 5
then add 
    new RCTCameraPackage()
after           
    new MainReactPackage(),
which will most likely be line 26 in the getPackages method.

in AndroidManifest.xml 
add     xmlns:tools="http://schemas.android.com/tools"
to line 2

then 
<uses-permission android:name="android.permission.CAMERA" />

    <uses-permission android:name="android.permission.RECORD_AUDIO"/>
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
in line 9

finally add 
      tools:node="replace"

in the application tag.

in androi\build.gradle
allprojects should look like this

allprojects {
    repositories {
        mavenLocal()
        jcenter()
        maven { url "https://jitpack.io" }
        maven {
                url "https://maven.google.com"
            }
        maven {
            // All of React Native (JS, Obj-C sources, Android binaries) is installed from npm
            url "$rootDir/../node_modules/react-native/android"
        }
    }
}


last but not least, for some reason, there's a chance that in settings.gradle gets reset so you will need to add

include ':react-native-camera'
project(':react-native-camera').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-camera/android')

after line 1